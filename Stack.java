package stack;

import java.util.ArrayList;

public class Stack {

    public class node {

        int item;
        node next;
    }
    node top;
    int size;

    boolean isEmpty() {
        return (top == null);
    }

    void push(int newitem) {
        size++;
        node p = new node();
        p.item = newitem;
        p.next = top;
        top = p;
    }

    int pop() {
        if (isEmpty()) {
            return -1;
        } else {
            size--;
            int x = top.item;
            top = top.next;
            return x;
        }
    }

    int numOfElement() {
        return size;
    }

    int search(int a) {
        node temp = top;
        int index = 0;
        while (temp != null) {
            if (temp.item == a) {
                return index + 1;
            } else {
                index++;
                temp = temp.next;
            }
        }
        return 0;
    }

    ArrayList<Integer> search1(int a) {

        node temp = top;
        ArrayList<Integer> stt = new ArrayList();
        int index = 0;
        while (temp != null) {
            index++;
            if (temp.item == a) {
                stt.add(index);
            }
            
            temp = temp.next;
        }
        if(stt.size()==0)
            stt.add(0);
        return stt;

    }

    void display() {
        if (isEmpty()) {
            System.out.println("Stack rong");
        } else {
            node temp = top;
            for (int i = 0; i < size; i++) {
                System.out.print(temp.item + " ");
                temp = temp.next;
            }
        }
    }
}