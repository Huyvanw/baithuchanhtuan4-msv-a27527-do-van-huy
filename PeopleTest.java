public class PeopleTest {

    public static void main(String[] args) {
//        Employee newbie = new Employee("Newbie", new MyDate(10, 2, 1989), 1000000);
//        System.out.println(newbie.toString());
//        Manager boss = new Manager("Boss", new MyDate(23, 2, 1979), 4000000);
//        boss.setAssistant(newbie);
//        System.out.println(boss.toString());
//        Manager bigBoss = new Manager("Big Boss", new MyDate(3, 12, 1969), 10000000);
//        bigBoss.setAssistant(boss);
//        System.out.println(bigBoss.toString());
        Person[] ps = new Person[3];
        ps[0]= new Employee("Newbie", new MyDate(10, 2, 1989), 1000000);
        ps[1]= new Manager("Boss", new MyDate(23, 2, 1979), 4000000, (Employee) ps[0]);
        ps[2]= new Manager("Big Boss", new MyDate(3, 12, 1969), 10000000, (Employee) ps[1]);
        for (int i = 0; i < ps.length; i++) {
            System.out.println(ps[i].toString());
        }
    }
}