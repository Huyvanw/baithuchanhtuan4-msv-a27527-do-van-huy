public class Employee extends Person{
        double salary;
        double getSalary(){
            return salary;
        }

        public Employee( String name, MyDate birthday,double salary) {
            super(name, birthday);
            this.salary = salary;
        }     
        @Override
        public String toString() {
            return super.toString() + " salary:"+salary;
        }
    }