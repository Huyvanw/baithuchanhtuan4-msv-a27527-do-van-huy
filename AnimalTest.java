public class AnimalTest {

    public static void main(String[] args) {
        Animal[] animals = new Animal[2];
        animals[0] = new Cow("Bo");
        animals[1] = new Cat("Meo");
        for (int i = 0; i < animals.length; i++) {
            animals[i].selfintroduce();
        }
    }