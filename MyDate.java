public class MyDate {
    int day;
    int month;
    int year;
   
    public MyDate(){
    }
    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
    //public static MyDate KhoiTao() {
    //     return new MyDate();
    // }
    //public static void KhoiTao(MyDate md){
    //    md=new MyDate();
    //} 

    @Override
    public String toString() {
        return  day + "/" + month + "/" + year;
    }
    
}