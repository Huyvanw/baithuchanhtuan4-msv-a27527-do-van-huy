public class Animal {

    public String name;

    public Animal() {
        this.name = "no name";
    }

    public Animal(String name) {
        this.name = name;
    }

    public String GetName() {
        return name;
    }

    public void sayHello() {
        System.out.println("Well...I don't know what to say ");
    } 
    public void selfintroduce() {
        sayHello();
        System.out.println("My name is " + name + " I am a " + this.getClass().getSimpleName());
    }
}