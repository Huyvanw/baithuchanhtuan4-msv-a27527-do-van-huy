public class Manager extends Employee {

    Employee assistant;

    public Manager(String name, MyDate birthday, double salary) {
        super(name, birthday, salary);
    }

    public Manager(String name, MyDate birthday, double salary, Employee assistant) {
        super(name, birthday, salary);
        this.assistant = assistant;
    }

    public void setAssistant(Employee e) {
        this.assistant = e;
    }

    @Override
    public String toString() {
        return super.toString() + " assistant: "+this.assistant;
    }
}